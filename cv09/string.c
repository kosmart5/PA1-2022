#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Initial capacity of an empty string
 */
#define STRING_INITIAL_CAPACITY 10
/**
 * String multiplicative growth factor
 */
#define STRING_GROW_FACTOR 2
/**
 * String constant growth factor
 */
#define STRING_GROW_ADDITION 10

/**
 * Dynamic C string with stored length
 */
typedef struct {
    /*
     * Char array with the string characters, and trailing 0
     * byte at the end for compatibility with standard library
     * functions
     */
    char * buffer;

    /**
     * Size of the dynamically allocated buffer
     */
    size_t capacity;

    /**
     * Number of chars stored in the string excluding the trailing
     * null byte. Should be equal to strlen(buffer)
     */
    size_t size;
} String;

/**
 * @brief Creates new empty String structure
 *
 * ALLOCATES buffer with STRING_INITIAL_CAPACITY size,
 * Sets buffer's first character to '\0' to indicate empty string.
 * @return new String with ALLOCATED buffer
 */
String createString() {
    String str;
    str.capacity = STRING_INITIAL_CAPACITY;
    str.buffer = (char *) malloc(sizeof(*str.buffer) * str.capacity);
    str.buffer[0] = '\0';
    str.size = 0;
    return str;
}

/**
 * @brief Append single char at the end of a string
 *
 * If needed, this method will REALLOCATE the buffer,
 * increasing its size based on the STRING_GROW_FACTOR
 * and STRING_GROW_ADDITION.
 *
 * @param[in,out] str String structure to append the char to
 * @param[in] ch      The char to append to the end of the string
 */
void appendChar(String * str, char ch) {
    if (str->size + 2 >= str->capacity) {
        str->capacity = str->capacity * STRING_GROW_FACTOR + STRING_GROW_ADDITION;
        str->buffer = (char *) realloc(str->buffer, sizeof(*str->buffer) * str->capacity);
    }

    str->buffer[str->size] = ch;
    str->buffer[str->size + 1] = '\0';
    str->size++;
}

/**
 * @brief Destroys dynamically allocated String
 *
 * Deallocates/frees buffer of chars and sets all variables into,
 * illegal state for easier debugging if, by any chance, accessed after destruction.
 *
 * @param[in,out] str String to deallocate/free/destroy
 */
void destroyString(String * str) {
    free(str->buffer);
    // Set buffer to NULL so that if we try to access freed memory the program should crash.
    str->buffer = NULL;
    str->capacity = 0;
    str->size = 0;
}

/**
 * @brief Sets String to empty string
 *
 * This method uses destroy(String *) and createString(),
 * first to destroy and free the current buffer, and then to create/allocate,
 * a new empty string.
 *
 * @param[in,out] str String to be set to empty.
 */
void emptyString(String * str) {
    destroyString(str);
    String temp = createString();
    *str = temp;
}

/**
 * Simple print function.
 *
 * The commented out if is present for legacy reasons, it was
 * necessary when an empty String struct was represented with
 * an invalid NULL buffer instead of a valid empty string.
 *
 * @param str[in]
 */
void printString(String * str) {
    // No longer needed since default String is initialized to empty String now
    // if (str->buffer == NULL) {
    //     printf("NULL\n");
    //     return;
    // }
    printf("%s\n", str->buffer);
}

/**
 * @brief Concatenates second string to the end of the first one
 *
 * This function will add the String * src
 * into the String * dest, using the appendChar method
 * which could result in reallocation of the dest buffer.
 *
 * TODO: (Optimize) Could be possibly optimised to realloc less often?
 * @param dest[in,out] String structure to append the src into
 * @param str[int] String to append
 */
void concatenate(String * dest, String * str) {
    for (size_t index = 0; index < str->size; index++) {
        appendChar(dest, str->buffer[index]);
    }
}

/**
 * @brief Creates String from C char array.
 *
 * Creates dynamically allocated String structure as a copy of C char array.
 * C char array must be ended with '\0' for this function to work.
 *
 * TODO: Think about how this could be further optimised to avoid single char appends
 *       Hint: man 3 strdup
 *
 * @param str[in] C string to copy into the structure
 * @return dynamically allocated String with copy of the str.
 */
String createStringFromCString(const char * str) {
    String smartStr = createString();
    size_t len = strlen(str);

    for (size_t index = 0; index < len; index++) {
        appendChar(&smartStr, str[index]);
    }

    return smartStr;
}

/**
 * @brief Creates a DEEP copy of String structure
 *
 * Creates new String structure, allocates new buffer,
 * and copies the char data from str into it.
 *
 * @param str[in] String structure to copy
 * @return dynamically allocated String with copy of the str.
 */
String copyString(String * str) {
    String copy = createString();
    // Viz video
    // for(size_t index = 0; index < str->size; index++) {
    //     appendChar(&copy, str->buffer[index]);
    // }
    concatenate(&copy, str);
    return copy;
}


/**
 * @brief Calculates the lowest index where `c` occurs in the given string
 *
 * @param str[in] the string to search in
 * @param c[in] the param to look for
 * @returns the index
 */
size_t findChar(String * str, char c);


/**
 * @brief Checks whether `pref` is a prefix of `str`
 *
 * @param str[in] the string to search in
 * @param pref[in] the prefix to look for
 * @returns 1 if `pref` is a prefix of `str`, 0 otherwise
 */
char hasPrefix(String * str, String * pref);


/**
 * @brief Find the first occurence of `substr` in `str`
 *
 * @param str[in] the string to search in
 * @param substr[in] the string to look for in `str`
 * @returns the lowest index at which `substr` occurs in `str`
 *
 * @discussion
 * For `str` of length n and `substr` of length `m` (`m <= n`) this can be done
 * naiively in O(n^2) time. For a challenge, try finding an algorithm
 * that can do it in O(n) time.
 */
size_t findSubstring(String * str, String * substr);


int main() {
    String first = createString();
    String last = createString();
    String copy = copyString(&first);

    char c;
    while (scanf("%c", &c) == 1 && c != ',') {
        appendChar(&first, c);
    }

    while (scanf("%c", &c) == 1) {
        appendChar(&last, c);
    }

    String comma = createStringFromCString(", ");

    String full = copyString(&first);
    concatenate(&full, &comma);
    concatenate(&full, &last);

    printf("First: ");
    printString(&first);
    printf("Last: ");
    printString(&last);
    printf("Full: ");
    printString(&full);
    printf("Copy: ");
    printString(&copy);

    destroyString(&first);
    destroyString(&last);
    destroyString(&full);
    destroyString(&comma);
    destroyString(&copy);
}

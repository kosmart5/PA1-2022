#include <stdio.h>
#include <assert.h>


#define FIB_TEST_MIN -5
#define FIB_TEST_MAX 25


/// Calculate the factorial of n
/// fact(n) = n * (n - 1) * (n - 2) * (n - 3) * ... * 2 * 1
/// fact(n) = n * fact(n - 1)
int fact (int n) {
    if (n <= 1) {
        return 1;
    }

    return n * fact(n - 1);
}


/// Calculate the n-th fibonacci number
/// fib(n) = fib(n - 1) + fib(n - 2)
/// fib(1) = 1, fib(2) = 1
int fib(int n) {
    printf("FIB %d\n", n);
    if (n <= 2) {
        return 1;

    }
    return fib(n - 1) + fib(n - 2);
}


long FIB[101];
/// Calculate the n-th fibonacci number
/// This function uses the FIB statically allocated array to avoid
/// repeatedly computing the values of fib(n). Due to the capacity
/// of FIB, this function can only handle values of n <= 100, but
/// that can be fixed for example with a wrapper function and
/// dynamically allocating a memoization array of the required
/// size.
long fibMemo(long n) {
    if (n <= 2) {
        return 1;
    }

    if (FIB[n] == 0) {
        printf("FIB MEMO %ld\n", n);
        FIB[n] = fibMemo(n - 1) + fibMemo(n - 2);
    }

    return FIB[n];
}


/// Test the functionality of the fibMemo function.
/// This is a simple tester which assumes that fib returns correct
/// values and uses it as a reference to test the faster fibMemo
/// function
void testFib () {
    for (int i = FIB_TEST_MIN; i <= FIB_TEST_MAX; ++i) {
        if (fib(i) != fibMemo(i)) {
            printf("Fib test failed for i = %d\n", i);
            assert(0);
        }
    }
}


/// Calculate the prime factorisation of n
void factorizeRec(int n, int d) {
    if (n == 1) {
        return;
    }

    if (n % d == 0) {
        printf("%d", d);
        if (n / d != 1) {
            printf(" * ");
        }

        factorizeRec(n / d, d);
    } else {
        factorizeRec(n, d + 1);
    }
}


/// Calculate the prime factorisation of n
void factorize(int n) {
    if (n == 1 || n == 0 || n == -1) {
        printf("%d = %d\n", n, n);
        return;
    }

    printf("%d = ", n);

    if (n < 0) {
        printf("-");
        n = -n;
    }

    factorizeRec(n, 2);
    printf("\n");
}


/// Calculate the exponent on a given factor of n
int calculateExponent(int * n, int d) {
    int exponent = 0;
    while (*n % d == 0) {
        exponent += 1;
        *n = *n / d;
    }
    return exponent;
}


/// Calculate the prime factorisation of n with exponents
/// for repeated divisors
void factorizeExponentsRec(int n, int d) {
    if (n == 1) {
        return;
    }

    if (n % d == 0) {
        int exponent = calculateExponent(&n, d);

        if (exponent == 1) {
            printf("%d", d);
        } else {
            printf("%d^%d", d, exponent);
        }

        if (n != 1) {
            printf(" * ");
        }
    }

    factorizeExponentsRec(n, d + 1);
}


/// Calculate the prime factorisation of n with exponents
/// for repeated divisors
void factorizeExponents(int n) {
    if (n == 1 || n == 0 || n == -1) {
        printf("%d = %d\n", n, n);
        return;
    }

    printf("%d = ", n);

    if (n < 0) {
        printf("-");
        n = -n;
    }

    factorizeExponentsRec(n, 2);
    printf("\n");
}
#include <stdlib.h>


int main() {
    // testFib();
    // printf("%d\n", fact(6));
    // printf("%d\n", fib(11));

    // printf("%ld\n", fibMemo(100));

    factorize(-1024 * 54);
    factorizeExponents(-1024 * 53);
    factorizeExponents(-(1<<10) * 53);


    void * k = malloc(1);
    free(k);
    realloc(k, 10);
}

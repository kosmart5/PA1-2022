#include <stdio.h>
#include <stdlib.h>


typedef struct Node {
    int value;
    struct Node * next;
    struct Node * prev;
} Node;

typedef struct List {
    Node * head;
    Node * tail;
    int size;
} List;


/**
 * Create a node with the given value
 * @param value The value the node should hold
 * @returns A `malloc`ed node with the given value
 */
Node * nodeCreate(int value) {
    Node * node = (Node *) malloc(sizeof(Node));
    node->value = value;
    node->next = 0;
    return node;
}


/**
 * Create an empty list
 */
List * listCreate() {
    List * list = (List *) malloc(sizeof(List));
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    return list;
}


void listAppend(List * lst, int value){
    Node * temp = nodeCreate(value);
    if(lst->size == 0) {
        lst->head = temp;
        lst->tail = temp;
        lst->size = 1;
    } else {
        lst->tail->next = temp;
        lst->tail = temp;
        lst->size += 1;
    }
}


void listPrepend(List * lst, int value){
    Node * temp = nodeCreate(value);
    if(lst->size == 0) {
        lst->head = temp;
        lst->tail = temp;
        lst->size = 1;
    } else {
        lst->head = temp;
        temp->next = lst->head;
        lst->size += 1;
    }
}



void listClear(List * lst) {
    Node * head = lst->head;
    while (head != NULL) {
        Node * next = head->next;
        free(head);
        head = next;
    }
    lst->head = NULL;
    lst->tail = NULL;
    lst->size = 0;
}

int listAt(List * lst, int index) {
    if(index >= lst->size) return -1;
    Node * head = lst->head;
    while(index > 0){
        head = head->next;
        index --;
    }
    return head->value;
}

void listPrint(List * lst) {
    Node * head = lst->head;
    
    while (head != NULL) {
        // Don't print the trailing "->"
        if (head != lst->head) printf("->");
        printf("%d", head->value);
        head = head->next;
    }

    printf("\n");
}


int listFind(List * lst, int value) {
    Node * head = lst->head;
    while (head != NULL) {
        if(head->value == value) return 1;
        head = head->next;
    }
    return 0;
}



int main() {
    List * lst = listCreate();
    listAppend(lst, 1);
    listAppend(lst, 2);
    listAppend(lst, 3);
    listAppend(lst, 4);

    listPrint(lst);

    printf("\n%d\n", listAt(lst, 3));

    listClear(lst);

    listAppend(lst, 9);
    listClear(lst);

    free(lst);

    return 0;
}


